package ${package};

import org.apache.camel.builder.RouteBuilder;

/**
 * Hello world!
 *
 */
public class APIRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("cxfrs:bean:api?bindingStyle=SimpleConsumer").log("you did it!");

    }
}
